<div class="wrap">
	<div class="wrapper container">
	    <div class="row">
	    	<div class="col-12">
	    		<nav class="navbar navbar-expand-lg">
				  	<a class="navbar-brand" href="#"><img id="mobile-logo" class="img-fluid" src="images/logo-mobile.png"></a>
				  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  	</button>
				  	<div class="collapse navbar-collapse" id="navbarNav">
					    <ul class="navbar-nav ml-auto">
					      	<li class="nav-item active text-center" onclick="scrollIt('about-section')">
					        	<a class="nav-link" href="#">About</a>
					      	</li>
					      	<li class="nav-item text-center" onclick="scrollIt('menu-section')">
				       	 		<a class="nav-link" href="#">Menu</a>
					      	</li>
					      	<li class="nav-item text-center" onclick="scrollIt('catering-section')">
					        	<a class="nav-link" href="#">Reviews</a>
					      	</li>
					      	<li class="nav-item text-center" onclick="scrollIt('location-section')">
					        	<a class="nav-link" href="#">Hours & Location</a>
					      	</li>
					      	<li style="margin-left: -10px" class="nav-item text-center">
					        	<a target="_blank" href="https://www.facebook.com/ByblosLebaneseCuisine/"><i class="fab fa-facebook"></i></a>
					        	<a href="https://www.instagram.com/bybloscuisine/?hl=en" target="_blank"><i class="fab fa-instagram"></i></a>
					        	<a href="https://www.tripadvisor.ca/Restaurant_Review-g154974-d10115117-Reviews-Byblos_Lebanese_Restaurant-Dartmouth_Halifax_Region_Nova_Scotia.html" target="_blank"><i class="fab fa-tripadvisor"></i></a>
					        	<a href="https://www.yelp.ca/biz/byblos-lebanese-cuisine-dartmouth?osq=byblos+restaurants" target="_blank"><i class="fab fa-yelp"></i></a>
					      	</li>
					      	<li class="nav-item text-center">
					      		<a class="nav-link" href="tel:9024355555">902-435-5555</a>
					      	</li>
					    </ul>
				  	</div>
				</nav>
	    	</div>
	    </div>
	</div>
</div>

		
<script>
	$(document).ready(function(){
		var sirina = $(window).width();

		if(sirina<576){
			$('.navbar-toggler').css('padding','0.2rem 0.45rem');
			$('.navbar-toggler').css('font-size','1.05rem');
			$('.wrap img').css('width','60%');
		}

		else if(sirina<767){
			$('.wrap img').css('width','80%');
		}


	});
</script>
			
		
						
					