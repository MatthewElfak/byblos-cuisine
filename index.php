<!DOCTYPE html>
<html>
<head>
	<title>Byblos Cuisine | Lebanese & Mediterranean Cuisine</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="byblos cuisine, byblos, lebanese cuisine, halifax, nova scotia, mediterranean food, mediterranean cuisine, cuisine halifax">
	<meta name="description" content="Byblos embraces the authentic Lebanese & Mediterranean cuisine and culture. Bringing in local ingredients to obtain the ultimate goal of fresh and delicious food. ">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<link rel="icon" href="images/icon.png">
	<link rel="stylesheet" type="text/css" href="assets/css/home.css">
	<link rel="stylesheet" type="text/css" href="assets/css/header.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/popper.min.js"></script>
	<script src="assets/js/scroll-navbar.js"></script>
	<script src="assets/js/scroll-to-section.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Galada" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/footer.css">
	<link rel="icon" href="images/icon.png">
	<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<style>
		body { font-family:'Open Sans'; background-color:#fafafa;}
		h2 { margin:30px auto;}

		#mixedSlider {
		  position: relative;
		}
		#mixedSlider .MS-content {
		  white-space: nowrap;
		  overflow: hidden;
		  margin: 0 5%;
		}
		#mixedSlider .MS-content .item {
		  display: inline-block;
		  width: 33.3333%;
		  position: relative;
		  vertical-align: top;
		  overflow: hidden;
		  height: 100%;
		  white-space: normal;
		  padding: 0 10px;
		}
		@media (max-width: 991px) {
		  #mixedSlider .MS-content .item {
		    width: 50%;
		  }
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-content .item {
		    width: 100%;
		  }
		}
		#mixedSlider .MS-content .item .imgTitle {
		  position: relative;
		}
		#mixedSlider .MS-content .item .imgTitle .blogTitle {
		  margin: 0;
		  text-align: left;
		  letter-spacing: 2px;
		  color: #252525;
		  font-style: italic;
		  position: absolute;
		  background-color: rgba(255, 255, 255, 0.5);
		  width: 100%;
		  bottom: 0;
		  font-weight: bold;
		  padding: 0 0 2px 10px;
		}
		#mixedSlider .MS-content .item .imgTitle img {
		  height: auto;
		  width: 100%;
		}
		#mixedSlider .MS-content .item p {
		  font-size: 16px;
		  margin: 2px 10px 0 5px;
		}
		#mixedSlider .MS-content .item a {
		  float: right;
		  margin: 0 20px 0 0;
		  font-size: 16px;
		  font-style: italic;
		  color: rgba(173, 0, 0, 0.82);
		  font-weight: bold;
		  letter-spacing: 1px;
		  transition: linear 0.1s;
		}
		#mixedSlider .MS-content .item a:hover {
		  text-shadow: 0 0 1px grey;
		}
		#mixedSlider .MS-controls button {
		  position: absolute;
		  border: none;
		  background-color: transparent;
		  outline: 0;
		  font-size: 50px;
		  top: 95px;
		  color: rgba(0, 0, 0, 0.4);
		  transition: 0.15s linear;
		}
		#mixedSlider .MS-controls button:hover {
		  color: rgba(0, 0, 0, 0.8);
		}
		@media (max-width: 992px) {
		  #mixedSlider .MS-controls button {
		    font-size: 30px;
		  }
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-controls button {
		    font-size: 20px;
		  }
		}
		#mixedSlider .MS-controls .MS-left {
		  left: 0px;
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-controls .MS-left {
		    left: -10px;
		  }
		}
		#mixedSlider .MS-controls .MS-right {
		  right: 0px;
		}
		@media (max-width: 767px) {
		  #mixedSlider .MS-controls .MS-right {
		    right: -10px;
		  }
		}
		#basicSlider { position: relative; }

		#basicSlider .MS-content {
		  white-space: nowrap;
		  overflow: hidden;
		  margin: 0 2%;
		  height: 50px;
		}

		#basicSlider .MS-content .item {
		  display: inline-block;
		  width: 20%;
		  position: relative;
		  vertical-align: top;
		  overflow: hidden;
		  height: 100%;
		  white-space: normal;
		  line-height: 50px;
		  vertical-align: middle;
		}
		@media (max-width: 991px) {

		#basicSlider .MS-content .item { width: 25%; }
		}
		@media (max-width: 767px) {

		#basicSlider .MS-content .item { width: 35%; }
		}
		@media (max-width: 500px) {

		#basicSlider .MS-content .item { width: 50%; }
		}

		#basicSlider .MS-content .item a {
		  line-height: 50px;
		  vertical-align: middle;
		}

		#basicSlider .MS-controls button { position: absolute; }

		#basicSlider .MS-controls .MS-left {
		  top: 35px;
		  left: 10px;
		}

		#basicSlider .MS-controls .MS-right {
		  top: 35px;
		  right: 10px;
		}

		.MS-left{
			top: 100px !important;
			color: #fff !important;
		}

		.MS-right{
			top: 100px !important;
			color: #fff !important;
		}

		.item p{
			font-style: italic !important;
		}
	</style>
</head>
<body>
	
	<?php require_once 'header.php'; ?>

	<div id="slides">
	    <div class="slides-container">
	      	<img src="images/fire-pit.jpg" alt="Lebanese Cuisine">
	      	<img src="images/sh.jpg" alt="Lebanese & Mediterranean Cuisine">
	      	<img src="images/humus.jpg" alt="Lebanese Cuisine Food">
	      	<img src="images/zadnji.jpg" alt="Halifax Lebanese Cuisine">
	    </div>
 	</div>

	<div class="container-fluid" id="about-section">
		<div class="row">
			<div class="container">
				<div class="col-12 text-center">
					<h1>About Us</h1>
					<p>Byblos embraces the authentic Lebanese & Mediterranean cuisine and culture. Bringing in local ingredients to obtain the ultimate goal of fresh and delicious food. Sourcing the beef, lamb, poultry and seafood, vegetables, cheese and strained yogurt as well as pita bread locally. Byblos is introducing the first <b style="color: #fff;">Lebanese charcoal pit</b> within the Halifax/Dartmouth area which focuses on a live view of the BBQ; encapsulating the true taste of the authentic Lebanese food. Dine in or take out Byblos offers a wide variety of mouth-watering dishes, including vegan menu. Come in and experience the atmosphere.</p>
					<br>
					<h2>We Support local farmers and businesses!</h2>
					<p><b class="orange">East Coast Bakery:</b> 6257 Quinpool Rd, Halifax * 902-429-6257</p>
					<p><b class="orange">Chater Meat Market:</b> 250 Wyse Rd, Dartmouth * 902-464-4777</p>
					<p><b class="orange">Bailey Halal House:</b> 1189 Bedford Hwy, Bedford * 902-835-8488</p>
					<p><b class="orange">Mike Oulton Meat Store - Martock Glen Farm:</b> 5246 Nova Scotia 14, Windsor * 902-798-4734</p>
					<p><b class="orange">Fisherman's Market:</b> 607 Bedford Hwy, Halifax * 902-443-3474</p>
					<p><b class="orange">Northumberland Lamb:</b> 2A6, 24 Brookside Branch Rd, Rible Hill * 902-895-4262</p>
					<p><b class="orange">Eden Valley Poultry:</b> 326 Main St, Berwick * 902-538-3800</p>
					<p><b class="orange">CTL Distributing - Vegetables:</b> 202 Brownlow Ave, Dartmouth * 902-876-6327</p>
					<p><b class="orange">The Golden Bakery:</b> 1350 Bedford Hwy, Bedford * 902-406-7482</p>
					<p><b class="orange">Holmestead Cheese:</b> RR#1 Alyesford, NS * 902-847-9034</p>
					<p><b class="orange">Our Own Blend of coffee roasted at Java Blend Coffee Roasters:</b> 6027 North St, Halifax * 902-423-6944</p>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid catering-section">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2>We Cater</h2>
					<p>For inquiries, please call 902-435-5555 or send us a message on <a target="_blank" href="https://www.facebook.com/ByblosLebaneseCuisine/">Facebook</a></p>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="container-fluid menu-wrapper">
	<div class="container" id="menu-section">
		<div class="row choose-menu">
			<div class="text-sm-center custom-offset col-12 col-sm-12 col-md-4 col-lg-3 selected"><button onclick="openMenu(this,'1');" class="btn" data-menu-number="1">Lunch Menu 11AM - 4PM</button></div>
			<div class="text-sm-center col-12 col-sm-12 offset-md-0 col-md-4 offset-lg-1 col-lg-3"><button onclick="openMenu(this,'2');" class="btn" data-menu-number="2">Evening Menu - After 4PM</button></div>
			<div class="text-sm-center col-12 col-sm-12 offset-md-0 col-md-4 offset-lg-1 col-lg-3 last-button"><button onclick="openMenu(this,'3');" class="btn" data-menu-number="3">Takeout/Pick-Up Only</button></div>
		</div>
	</div>

	<div class="container">
<!--//////////////////////////BYBLOS MEZZETA/////////////////////////-->
		<div class="row first-row align-items-center menu2">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center long3">Byblos Mezzet Appetizers</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-12 text-center"><h2 class="text-center addon">*All mezzet served with pita bread. V - Vegetarian, GF - Gluten Free*</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Hummus (V, GF)</h2><h6>Chickpeas paste, tahini, fresh lemon, garlic drizzled with olive oil</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>7</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Byblos Mezzet</h2><h6>A sampling of selectiong of Chef's delicious hold and cold appetizers</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>13</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Baba Ghanouj (V, GF)</h2><h6>Smoked eggplant, tahini, garlic, fresh lemon drizzled with olive oil</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>7</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Grape Leaves (V, GF)</h2><h6>Home made grape leaves stuffed w/rice, fresh herbs, tomatoes, Italian parsley</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>8</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Aubergine Sauté (V, GF)</h2><h6>Eggplant sautéed in savoury tomato onion garlic sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>7</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Kebbe</h2><h6>Best cut of ground beef mixed with bulgur (cracked wheat) stuffed with meat, onion, spiced to flavour</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Falafel (V, GF)</h2><h6>Herbed chickpeas vegetable patties with garnish and tahini sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>7</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Kebbe Végétarien (V, GF)</h2><h6>Pumpkin grounded with bulgur (cracked wheat) stuffed with chickpeas, sautéed onion, spinach to flavour</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Byblos Birds Nest (GF)</h2><h6>Hummus topped with friend pieces of tender meat house specialty</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Garlic Fries</h2><h6>Fresh hand cut friend PEI potatoes served with our signature garlic sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Fatayer of Your Choice </h2><h6>*Home made pastry stuffed with spinach onion, fresh lemon spices (baked)</h6>
				<h6>*Home made pastry stuffed with cheese, herbs and olive oil (fried)</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>7</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Calamari</h2><h6>Lightly breaded and friend with lemon herbs served with home dipping sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>10</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Octopus</h2><h6>Prepared the Mediterranean way on a nice garlic sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>10</h2></div>
		</div>
<!--///////////////////////////////SHWARMA/////////////////-->

		<div class="row first-row align-items-center menu2">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Shawarma</div>
			<div class="offset-1 offset-sm-0 col-2 line text-center"></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma</h2><h6>Marinated chicken in special organic herbs and spices served with rice, garlic potatoes, choice of salad, and garlic sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>16</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Beef Shawarma</h2><h6>Marinated tender cuts of beef in special organic herbs and spices, served with rice, garlic potatotes and choice of salad</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>16</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Mixed Chicken & Beef Shawarma</h2><h6>Marinated tender cuts of beef and chickend in special organic herbs and spices, served with rice, garlic potatotes and choice of salad (tehini sauce, garlic sauce)</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>17</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Chicken Poutine Shawarma</h2><h6>Hand cut fries, chicken shawarma topped with cheese and house made gravy (garlic optional) and choice of salad</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>16</h2></div>
		</div>

		<!--///////////////////////////////Burgers/////////////////-->

		<div class="row first-row align-items-center menu2">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Burgers</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-12 text-center"><h2 class="text-center addon">*All burgers are served with hand cut garlic friend and choice of salad*</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Falafel Burger</h2><h6>House-made golden falafel burger, toppped with tahini sauce, hummus, tabouli, lettuce, red onion, pickles, and tomato</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>16</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Chicken Shawarma Burger Platter</h2><h6>Spit roasted marinated chicken on a gourment bun, topped with garlic sauce, tomato, red onion , and lettuce</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>16</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Lamb Kofta or Beef Kofta Burger</h2><h6>Freshly ground Nova Scotia lamb burger on a gorumet bun, topped with tzatziki, feta cheese, pickles, tomato, red onion , and lettuce. Or Beef burger made fresh on a gourmet bun, topped with smoked gouda, pickles, Byblos house-made special sauce, tomato, red onion, and lettuce</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>16</h2></div>
		</div>

		<!--///////////////////////////////SALATA/////////////////-->

		<div class="row first-row align-items-center menu2">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Salata</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-12 text-center"><h2 class="text-center addon">*Add grilled chickend $8, grilled lamb $9 or grilled seafood $9. All on charcoal*</h2></div>
			<div class="col-12 text-center"><h2 class="text-center addon">V - Vegetarian, GF - Gluten Free</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Tabouli Salata (V)</h2><h6>Chopped Italian parsley, diced tomatoes, gree onion, fresh mint, bulgur (chacked wheat) fresh lemon and olive oil</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Feta Cheese Lover (V, GF)</h2><h6>Crumbled Feta cheese with vine tomatoes, green onion ,drizzled olive oil</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Fattoush Salata (V)</h2><h6>Fresh romaine heart, cucumber, bell pepper, green and red onion, tomato, Italian parsley, fresh mint topped with fried pita pieces, lemon grenadine, olive oil</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Feta Cheese Salata (V, GF)</h2><h6>Fresh romaine heart, green red onion, vine tomatoes, red and bell peppers, vinegar, olive oil and topped with cumbled Feta cheese</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9</h2></div>
		</div>

		<!--///////////////////////////////Kebabs and mains/////////////////-->

		<div class="row first-row align-items-center menu2">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center long3">Byblos Kebabs & Mains</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-12 text-center"><h2 class="text-center addon">*Served with our special home made Byblos Rice, Grilled Veggies*</h2></div>
			<div class="col-12 text-center"><h2 class="text-center addon">V - Vegetarian, GF - Gluten Free</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kebab Végétarien (V, GF)</h2><h6>Fresh grilled vegetables finished with eggplant, sauted savoury tomatoes, onion, garlic sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>17</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Végétarien Platter (V, GF)</h2><h6>Spinach & cheese fatayer, falafel, vegetarian kebbe, and sautéed aubergine (eggplant) </h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>17</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Spicy Chicken Kofta Kebab (GF)</h2><h6>Ground grain-fed chicken breast blended with herbs and spices</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Chicken Kebab (Shish Tawook) (GF)</h2><h6>Tender cubes of grain-fed chicken breast marinated in our special organic sauce</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>17</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Byblos Lamb Kofta Kebab (GF)</h2><h6>Ground Nova Scotia lamb mixed with organic herbs and spices</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Lamb Shish Kebab (GF)</h2><h6>Tender cubes of Nova Scotia lamb marinated in organic herbs and olive oil</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>17</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Beef Shish Kebab (GF)</h2><h6>Tender cubes of Nova Scotia beef (AAA) marinated in organic herbs and olive oil</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>17</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Byblos Beef Kofta Kebab (GF)</h2><h6>Ground Nova Scotia beef (AAA) mixed with organic herbs and spices</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>17</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Lamb Cutlets (GF)</h2><h6>Lighly seasoned Nova Scotia lamb chops grilled to perfection</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>20</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Lamb Medallions (GF)</h2><h6>Tenderloin of Nova Scotia lamb medallions grilled to perfection</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>20</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Mixed Kebab (GF)</h2><h6>Chef's selection of cuts of grain-fed chicken breast and Nova Scotia lamb - a great way of sampling the best flavours from our grill</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>22</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Byblos Featured Cut</h2><h6>Your favoured steak seasoned, grilled to perfection on charcoal. Ask your waitress.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>20</h2></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chef's Table</h2><h6>A mixed selection of chef's delicious hot & cold appetizers, Tabouli and Fatoush salad, mixed grilled meat (Lamb, Beef, & Chicken).</h6><h6><b>Add Kebbe Nayeh for $10 per person</b></h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>45</h2><h6 class="col-12 text-left" style="font-size: 0.8rem; padding:0px !important;">Per Person</h6></div>
		</div>

		<!--// Sea food -->

		<div class="row first-row align-items-center menu2">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Seafood</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Grilled Mediterranean Whole Fish</h2><h6>Brazino (Mediterranean Sea Bass) with hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>25</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Seafood Mix</h2><h6>Nova Scotia scallops & Atlantic salmon marinated, organic herbs and spices, fresh lemon olive oil, frilled to perfection topped with original house sauce and hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>19</h2></div>
		</div>
		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Haddock Tips *NEW*</h2><h6>Chef made, fresh Nova Scotia haddock tips with hand cut garlic fries and shoice of salad.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>16</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Haddock *NEW*</h2><h6>Chef made, fresh Nova Scotia haddock fillets (2 pcs) with hand cut garlic fries and choice of salad.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>16</h2></div>
		</div>
		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Calamari *NEW*</h2><h6>Chef sut and braded in house with hand cut garlic fries and choice of salad.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>16</h2></div>
		</div>


		<!--// Desserts and Coffe -->
		<div class="row first-row align-items-center menu2">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center long2">Desserts & Coffee</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Assorted Baklava</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Bistro Coffee</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>2.50</h2></div>
		</div>
		<div class="row align-items-center second-menu menu2">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Byblos Dessert Specialty</h2><h6>Ask server</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Assorted Tea</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>2.50</h2></div>
		</div>


<!--//////////////////////////PLATTERS/////////////////////////-->

		<div class="row first-row align-items-center">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Platters</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma</h2><h6>Chicken shawarma, house salad, rice, hand cut garlic fries, hummus, picle turnips, (garlic sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Beef Shawarma</h2><h6>Beef shawarma, house salad, rice, tendet cut of meat, hand cut garlic fries, hummus, pickle turnips, (garlic sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Mixed Shawarma</h2><h6>Chicken & beef shawarma, house salad, rice, hand cut garlic fries, hummus, pickle turnips, (garlic & tahini sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>10.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Chicken Kabab (Shish Tawook)</h2><h6>Tender cubes of marinated chicken breast, rice, hand cut garlic fries, house salad, hummus, pickle turnip, (garlic sauce), pita. Extra skewer of meat $3.00</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kafta Kabab (Choice of Lamb, Beef or Chicken)</h2><h6>Minced meat blended with herbs & spices, olive oil, hummus, rice, hand cut garlic fries, house salad, pickle turnip, pita. Extra skewer of meat $3.00</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Chicken Shawarma Poutine</h2><h6>Hand cut fries, chicken shawarma topped with cheese and house made gravy. (Garlic Optional)</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Falafel Platter (V)</h2><h6>Three golden falfel, hummus, house salad, rice or lentil rice, pickle turnip (tahini sauce), hand cut galick fries, pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Byblos Mezzet (V)</h2><h6>Two golden falafels, two grapes leaves, hummus, hand cut garlic fries, rice, house salad, pickle turnips (tahini sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>


<!-- ////////////////////////////THIRD-menu////////////////////////-->


		<div class="row align-items-center third-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma</h2><h6>Chicken shawarma, house salad, rice, hand cut garlic fries, hummus, picle turnips, (garlic sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Beef Shawarma</h2><h6>Beef shawarma, house salad, rice, tendet cut of meat, hand cut garlic fries, hummus, pickle turnips, (garlic sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Mixed Shawarma</h2><h6>Chicken & beef shawarma, house salad, rice, hand cut garlic fries, hummus, pickle turnips, (garlic & tahini sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>10.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Chicken Kabab (Shish Tawook)</h2><h6>Tender cubes of marinated chicken breast, rice, hand cut garlic fries, house salad, hummus, pickle turnip, (garlic sauce), pita. Extra skewer of meat $3.00</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kafta Kabab (Choice of Lamb,Beef or Chicken)</h2><h6>Minced meat blended with herbs & spices, olive oil, hummus, rice, hand cut garlic fries, house salad, pickle turnip, pita. Extra skewer of meat $3.00</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Chicken Shawarma Poutine</h2><h6>Hand cut fries, chicken shawarma topped with cheese and house made gravy. (Garlic Optional)</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Falafel Platter (V)</h2><h6>Three golden falfel, hummus, house salad, rice or lentil rice, pickle turnip (tahini sauce), hand cut galick fries, pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Byblos Mezzet (V)</h2><h6>Two golden falafels, two grapes leaves, hummus, hand cut garlic fries, rice, house salad, pickle turnips (tahini sauce), pita.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>


		<div class="row first-row align-items-center">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Seafood</div>
			<div class="col-2 line text-center"></div>
		</div>

<!--//////////////////////////SEAFOOD/////////////////////////-->

		<div class="row align-items-center first-menu">
			<div class="col-12 text-center"><h2 class="text-center addon">Add a salad for $2.50</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Haddock Tips</h2><h6>Chef made fresh Nova Scotia Haddock tips with hand cut garlic fries. Add a salad for $2.50</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>11.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Haddock</h2><h6>Chef made fresh Nova Scotia Hoddock fillet (2pcs) with hand cut garlic fries. Add a salad for $2.50</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>11.95</h2></div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Calamari</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>11.95</h2></div>
		</div>


		<div class="row align-items-center third-menu">
			<div class="col-12 text-center"><h2 class="text-center addon">*Substitute any salad for $2.50*</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Haddock Tips</h2><h6>Chef made fresh Nova Scotia Haddock tips with hand cut garlic fries. Add a salad for $2.50</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>11.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item menu-item"><h2>Haddock</h2><h6>Chef made fresh Nova Scotia Hoddock fillet (2pcs) with hand cut garlic fries. Add a salad for $2.50</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>11.95</h2></div>
		</div>

		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Calamari</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>11.95</h2></div>
		</div>


		<!--//////////////////////////WRAPS/////////////////////////-->

		<div class="row first-row align-items-center">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Wraps</div>
			<div class="col-2 line text-center"></div>
		</div>


		<div class="row align-items-center first-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-8 offset-sm-0 col-sm-3 offset-md-0 col-md-3 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken or Beef Shawarma</h2><h6></h6></div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Reg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.95</h2> 
			</div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>8.95</h2> 
			</div>
			<div class="col-8 offset-sm-1 col-sm-3 offset-md-1 col-md-3 offset-lg-0 col-lg-3 text-left menu-item"><h2>Falafel (V)</h2><h6></h6></div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Reg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.95</h2> 
			</div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>8.95</h2> 
			</div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="align-self-center col-8 offset-sm-0 col-sm-3 offset-md-0 col-md-3 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kafta Kebab (Choice of Lamb, Beef or Chicken)</h2><h6>Choice of Lamb, Beef, or Chicken</h6></div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Reg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.95</h2> 
			</div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>9.50</h2> 
			</div>

			<div class="col-8 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-0 col-lg-4 text-left menu-item"><h2>Dessert</h2><h6>Sweet Rice Pudding</h6></div>
			<div class="offset-1 col-2 text-center offset-sm-0 col-sm-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>3.50</h2></div>
		</div>


	
		<div class="row align-items-center third-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-8 offset-sm-0 col-sm-3 offset-md-0 col-md-3 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken or Beef Shawarma</h2><h6></h6></div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Reg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.95</h2> 
			</div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>8.95</h2> 
			</div>
			<div class="col-8 offset-sm-1 col-sm-3 offset-md-1 col-md-3 offset-lg-0 col-lg-3 text-left menu-item"><h2>Falafel (V)</h2><h6></h6></div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Reg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.95</h2> 
			</div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>8.95</h2> 
			</div>
		</div>

		<div class="row align-items-center third-menu">
			<div class="align-self-center col-8 offset-sm-0 col-sm-3 offset-md-0 col-md-3 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kafta Kebab (Lamb, Beef or Chicken)</h2><h6>Choice of Lamb, Beef, or Chicken</h6></div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Reg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>6.95</h2> 
			</div>
			<div class="col-2 col-sm-1 align-self-center price">
				<h2 class="col-12 text-left text-sm-center two-size">Lg.</h2>
				<h2 class="two-size"><span style="margin-top: 2px;">$</span>9.50</h2> 
			</div>

			<div class="col-8 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-0 col-lg-4 text-left menu-item"><h2>Dessert</h2><h6>Sweet Rice Pudding</h6></div>
			<div class="offset-1 col-2 text-center offset-sm-0 col-sm-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>3.50</h2></div>
		</div>

		


		<!--//////////////////////////WRAP PLATTERS///////////////////-->

		<div class="row first-row align-items-center">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center long2">Wrap Platters</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma</h2><h6>Marinated chicken breast roasted slowly (garlic sauce), house salad, hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Beef Shawarma</h2><h6>Marinated beef roasted slowly garnished with sumac onion, parsley (tahini sauce), house salad, han cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kafta Shawarma</h2><h6>Minced meat blended with herbs and spices (chili sauce) house salad & hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Falafel (V)</h2><h6>Chickpeas, vegetable patties deep fried (tahini sauce), house salad & hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>


		<div class="row align-items-center third-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma</h2><h6>Marinated chicken breast roasted slowly (garlic sauce), house salad, hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Beef Shawarma</h2><h6>Marinated beef roasted slowly garnished with sumac onion, parsley (tahini sauce), house salad, han cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Kafta Shawarma</h2><h6>Minced meat blended with herbs and spices (chili sauce) house salad & hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Falafel (V)</h2><h6>Chickpeas, vegetable patties deep fried (tahini sauce), house salad & hand cut garlic fries.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>




		<!--//////////////////////////BURGERS/////////////////////////-->

		<div class="row first-row align-items-center">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center">Burgers</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2></div>
			<div class="col-12 text-center"><h2 class="text-center addon">*All burgers are served with hand cut garlic fries*</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Lamb Kafta Burger</h2><h6>Freshly ground Nova Scotia lamb burger on a gourment bun, topped with tzatziki, feta cheese, pickle, tomato,  red onion and lettuce</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Beef Kafta Burger</h2><h6>Beef burger made fresh on a gourment bun, topped with smoked gouda, pickle, Byblos house made special sauce, tomato, red onion and lettuce.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma Burger Platter</h2><h6>Spit roasted marinated chicken on gourmet bun, topped with garlic sauce, tomato, red onion and lettuce.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Falafel Veggie Burger (V)</h2><h6>House-made golden falafel burger, topped with tahini sauce, hummus, tabouli, lettuce, red onion, pickles and tomato.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>


		<div class="row align-items-center third-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2></div>
			<div class="col-12 text-center"><h2 class="text-center addon">*All burgers are served with hand cut garlic fries*</h2><h2 style="margin-top: -30px; margin-bottom: 50px;">Add a salad for $2.50</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Lamb Kafta Burger</h2><h6>Freshly ground Nova Scotia lamb burger on a gourment bun, topped with tzatziki, feta cheese, pickle, tomato,  red onion and lettuce</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Beef Kafta Burger</h2><h6>Beef burger made fresh on a gourment bun, topped with smoked gouda, pickle, Byblos house made special sauce, tomato, red onion and lettuce.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>
		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Chicken Shawarma Burger Platter</h2><h6>Spit roasted marinated chicken on gourmet bun, topped with garlic sauce, tomato, red onion and lettuce.</h6></div>
			<div class="col-1 align-self-center price"><h2><span id="nesto" style="margin-top: 2px;">$</span>9.95</h2></div>
			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Falafel Veggie Burger (V)</h2><h6>House-made golden falafel burger, topped with tahini sauce, hummus, tabouli, lettuce, red onion, pickles and tomato.</h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>9.95</h2></div>
		</div>



		<!--//////////////////////////APPET/////////////////////////-->

		<div class="row first-row align-items-center">
			<div class="col-2 offset-1 line text-center"></div>
			<div class="col-6 category text-center long">Appetizers & Salads</div>
			<div class="col-2 line text-center"></div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Fatoush Salad (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Tabouli Salad (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Feta Cheese Salad (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Hummus Dip (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center first-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Baba Ghanouj (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Lentils & Rice (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center third-menu">
			<div class="col-12 text-center"><h2 class="text-center addon"> V - Vegetarian</h2></div>
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Fatoush Salad (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Tabouli Salad (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Feta Cheese Salad (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Hummus Dip (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>

		<div class="row align-items-center third-menu">
			<div class="col-9 offset-sm-0 col-sm-4 offset-md-0 col-md-4 offset-lg-1 custom-offset2 col-lg-4 text-left menu-item"><h2>Baba Ghanouj (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>

			<div class="col-9 offset-sm-1 col-sm-4 offset-md-1 col-md-4 offset-lg-1 col-lg-4 text-left menu-item"><h2>Lentils & Rice (V)</h2><h6></h6></div>
			<div class="col-1 align-self-center price"><h2><span style="margin-top: 2px;">$</span>7</h2></div>
		</div>
	</div>
</div>


<div class="container-fluid" id="catering-section">
	<div class="container" id="catering-section">
		<div class="row" style="margin-bottom: 25px; color: #D4AF37;">
			<div class="col-12 text-center">
				<h1>Reviews</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div id="mixedSlider">
				  <div class="MS-content">
				      <div class="item">
				          <p>First time visiting Byblos and it will NOT be the last! The food is so very delicious, easily the best calamari we have ever had. The seafood and kabobs are prepared on an open flame, along with the vegetables, excellent flavours. And the baklava: magnificent! You have to eat here! Thanks to the staff for incredible service (two personal table visits from the chef himself).</p><br>
				          <h4 style="text-align: right; margin-right: 15px;">-- Bradley Dillman</h4>
				      </div>
				      <div class="item">
				          <p>After a long week, my husband and i ordered delivery tonight. The food was incredible, arrived hot at the door in a very short time. The staff went above and beyond to make sure our meal and our evening was perfect! Thank you Byblos for the incredible service! We will definitely come back and let everyone know how amazing our experience was.</p><br>
				          <h4 style="text-align: right; margin-right: 15px;">-- Marlene Gillis</h4>
				      </div>
				      <div class="item">
				          <p>Some of the best food offered in the city is here at Byblos restaurant. I have ate here many times and everything I have ordered off the menu never disappoints. The meat is very good quality and so much flavour! Also, this restaurant has the best hummus in the city and great vegetarian options as well. Lastly, great service and atmosphere I highly recommend you go!</p><br>
				          <h4 style="text-align: right; margin-right: 15px;">-- Omar Kadray</h4>
				      </div>
				      <div class="item">
				          <p>A friend and I ate here for the first time, last night, and both had a very pleasant experience! The service was very welcoming and professional, the food was delicious, and the restaurant itself is very clean and family-friendly! Great spot to eat at, if you haven't already!!</p><br>
				          <h4 style="text-align: right; margin-right: 15px;">-- Monique Gracie</h4>
				      </div>
				      <div class="item">
				          <p>I went yesterday for the first time at lunch with a friend and I absolutely loved it! I had a 7 month old and a 5 year old with me. The server was very friendly and accommodating to my kids. We had the chicken shawarma plate with the hummus appetizer and it was delicious!</p><br>
				          <h4 style="text-align: right; margin-right: 15px;">-- Nathalie Poirier-Schofield</h4>
				      </div>
				  </div>
				  <div class="MS-controls">
				      <button class="MS-left"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
				      <button class="MS-right"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
				  </div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script> 
<script src="assets/js/multislider.js"></script> 
<script>
$('#basicSlider').multislider({
			continuous: true,
			duration: 5000
		});
		$('#mixedSlider').multislider({
			duration: 750,
			interval: 5000
		});
</script>

	





	<script>
		$(document).ready(function(){
			$('.second-menu').css('display','none');
			$('.third-menu').css('display','none');
		});

	</script>

	<script>

		var div = $('.category').toArray();

		function openMenu(dis,prosledjen) {

			$('.choose-menu div button').css('background-color','rgba(0,0,0,0.1)');

			$('.choose-menu div button').css('border','1px dashed rgba(0,0,0,.66)');

			if(prosledjen==1){
				$('.first-menu').css('display','flex');
				$('.third-menu').css('display','none');
				$('.second-menu').css('display','none');
				div.forEach(function(entry){
					entry.parentElement.style.display ="flex";
				});
				$('.menu2').css('display','none');
				dis.style.backgroundColor= "rgba(0,0,0,0.18)";
				dis.style.border= "1px solid rgba(0,0,0,.56)";
			}
			else if(prosledjen==2){
				$('.second-menu').css('display','flex');
				$('.first-menu').css('display','none');
				$('.third-menu').css('display','none');
				div.forEach(function(entry){
					entry.parentElement.style.display ="flex";
				});
				div[7].parentElement.style.display ="none";
				div[8].parentElement.style.display ="none";
				div[9].parentElement.style.display ="none";
				div[10].parentElement.style.display ="none";
				div[11].parentElement.style.display ="none";
				div[12].parentElement.style.display ="none";
				dis.style.backgroundColor= "rgba(0,0,0,0.18)";
				dis.style.border= "1px solid rgba(0,0,0,.56)";
			}
			else if(prosledjen==3){
				$('.third-menu').css('display','flex');
				$('.second-menu').css('display','none');
				$('.first-menu').css('display','none');
				div.forEach(function(entry){
					entry.parentElement.style.display ="flex";
				});
				$('.menu2').css('display','none');
				dis.style.backgroundColor= "rgba(0,0,0,0.18)";
				dis.style.border= "1px solid rgba(0,0,0,0.56)";
			}
		}
			
	</script>

	<div class="container-fluid">
		<div class="container" id="location-section">
			<div class="row">
				<div class="col-12 text-center">
					<h1>Hours & Location</h1>
					<p class="sub-title">Byblos Lebanese Cuisine</p>

					<div class="row wrap-line">
						<div class="col-5 line"></div>
						<div class="col-2 text-center icon"><i class="fas fa-link"></i></div>
						<div class="col-5 line"></div>
					</div>

					
					<p class="info"><b>Address: </b><a style="text-decoration: none; color: #000;" target="_blank" href="https://www.google.com/maps/place/644+Portland+St,+Dartmouth,+NS+B2W+2M3,+%D0%9A%D0%B0%D0%BD%D0%B0%D0%B4%D0%B0/@44.6710941,-63.5287822,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a2388f935a851:0x4b2611d131df3f83!8m2!3d44.6710941!4d-63.5265935">644 Portland St, Dartmouth, NS B2W 2M3, Canada</a></p>
					<p class="info"><b>Hours: </b>Monday - Saturday <b>11:30 am</b> - <b>9:00 pm</b>, Sunday <b>4:00 pm</b> - <b>9:00 pm</b></p>
					<p class="info">Byblos Lebanese Cuisine © 2018. Privacy Policy</p>
				</div>
			</div>
		</div>
</div>


	<div class="container-fluid" id="map-section">
		<div class="row">
			<div class="col-12 map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2837.3762491945404!2d-63.528782184047!3d44.671094079099745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b5a2388f935a851%3A0x4b2611d131df3f83!2zNjQ0IFBvcnRsYW5kIFN0LCBEYXJ0bW91dGgsIE5TIEIyVyAyTTMsINCa0LDQvdCw0LTQsA!5e0!3m2!1ssr!2srs!4v1530536567924" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>

	 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	  <script src="assets/js/jquery.easing.1.3.js"></script>
	  <script src="assets/js/jquery.animate-enhanced.min.js"></script>
	  <script src="assets/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>
	  <script>

	    $(function() {
	      $('#slides').superslides({
	        play: 7000
	      });
	    });
	  </script>

	<?php 

		if($_SERVER['REQUEST_METHOD'] == "POST"){

			$name = $_POST['name'];
			$email = $_POST['email'];
			$messages = $_POST['message'];

			
			$to = "";
			$subject = "";

			$message = "Name: ".$name." -- ";
			$message .= "Email: ".$email." -- ";
			$message .= "Message: ".$messages;
			$headers = "From: " . "\r\n" .
			"CC: ";

			mail($to, $subject, $message, $headers);

		}

	?>


	<?php require_once 'footer.php'; ?>	

</body>
</html>